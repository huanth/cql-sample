/* global AJS */

AJS.toInit(function () {
    $('#cql-search-button').click(function (e) {
        var listPage = [];//list page return by cql
        var option = '';//render result
        var strPage = '';//str get pageID, spaceKey to java
        var spaceTitle = $('#spaceTitle').val();
        var spaceCategory = $('#spaceCategory').val();
        var pageTitle = $('#pageTitle').val();
        var pageLabel = $('#pageLabel').val();
        var cqlString = '';
        $('#multiResultWereSearch option').remove();
        $('#count-result').text('');

        if (spaceTitle.length == 0 && spaceCategory.length == 0 && pageTitle.length == 0 && pageLabel.length == 0) {
            option += '<option class=\"message-panel message-handler\" style=\'margin:10px;\' disabled=\"disabled\">';
            option += AJS.I18n.getText('imlv.vn.btx.view.searchbycqlsample.select.search.result.option');
            option += '</option>';
            $('#multiResultWereSearch').append(option);
            $('#count-result').append(0);
            return false;
        }

        if (spaceTitle.length !== 0 || pageTitle.length !== 0) {
            cqlString = 'type=page';
        }
        if (spaceTitle.length !== 0) {
            cqlString += "%20and%20";
            cqlString += "(space.title~\"/.*" + spaceTitle + ".*/\" or space.title~\"" + spaceTitle + "\")";
        }
        if (pageTitle.length !== 0) {
            cqlString += "%20and%20";
            cqlString += "(title~\"/.*" + pageTitle + ".*/\" or title~\"" + pageTitle + "\")";
        }

        if (cqlString.length < 1) {
            $.ajax({
                async: false,
                type: 'POST',
                url: AJS.contextPath() + "/searchPage/byCategoriesAndLabel.action",
                dataType: "json",
                data: {
                    spaceCategory: spaceCategory,
                    pageLabel: pageLabel,
                    pagesByTitle: listPage
                },
                success: function (returnData) {
                    if (returnData.listPages.length !== 0) {
                        option += returnData.listPages;
                    }
                    $('#multiResultWereSearch').append(option);
                    $('#count-result').append(returnData.countPages);
                },
                error: function (xhr, httpStatusMessage, customErrorMessage) {
                    alert(customErrorMessage);
                }
            });
        } else {
            cqlString += "&limit=1000";
            $.ajax({
                async: false,
                type: 'GET',
                url: AJS.contextPath() + "/rest/api/content/search?cql=" + cqlString,
                dataType: "json",
                data: {},
                success: function (pages) {
                    //return spaces has category
                    //var listPage = [];
                    if (pages.results.length !== 0) {
                        $.each(pages.results, function (i, v) {
                            strPage = '';
                            strPage += '{"id":"' + v.id + '","_expandable":{"space":"' + v._expandable.space + '"}}';
                            listPage.push(strPage);
                        });
                        $.ajax({
                            async: false,
                            type: 'POST',
                            url: AJS.contextPath() + "/searchPage/byCategoriesAndLabel.action",
                            dataType: "json",
                            data: {
                                spaceCategory: spaceCategory,
                                pageLabel: pageLabel,
                                pagesByTitle: listPage
                            },
                            success: function (returnData) {
                                if (returnData.listPages.length !== 0) {
                                    option += returnData.listPages;
                                }
                                $('#multiResultWereSearch').append(option);
                                $('#count-result').append(returnData.countPages);
                            },
                            error: function (xhr, httpStatusMessage, customErrorMessage) {
                                alert(customErrorMessage);
                            }
                        });
                    } else {
                        option += '<option class=\"message-panel message-handler\" style=\'margin:10px;\' disabled=\"disabled\">';
                        option += AJS.I18n.getText('imlv.vn.btx.view.searchbycqlsample.select.search.result.option');
                        option += '</option>';
                        $('#multiResultWereSearch').append(option);
                        $('#count-result').append(0);
                    }
                }
            });
        }
    });
});