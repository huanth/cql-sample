package imlv.vn.sample.factory;

import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.util.ConfluenceHomeGlobalConstants;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author TamPT
 */
public class ManualLogUtil {

    public static final String MEMSOURCE_FOLDER_NAME = "WikiworksManualVersionLog";
    public static final String MEMSOURCE_FILE_NAME = "WikiworksManualVersion";
    public static final String MEMSOURCE_FILE_EXTENSION = "txt";
    private static BootstrapManager bootstrapManager;
    private static String resultPath = "";

    public static void outputLogException(Class className, Exception ex) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));
        try {
            outputLog(errors.toString());
        } catch (Exception ex1) {
            java.util.logging.Logger.getLogger(ManualLogUtil.class.getName()).log(Level.SEVERE, null, ex1);
        }
        Logger logger = LoggerFactory.getLogger(className);
        logger.error(ex.getMessage(), ex);
    }

    public static Map<String, Object> getBeanFaild(Exception ex) {
        Map<String, Object> mapBean = new HashMap<String, Object>();
        mapBean.put("result", false);
        mapBean.put("message", ex.getMessage());
        return mapBean;
    }

    public String getResultPath() {
        return resultPath;
    }

    public void setResultPath(String aResultPath) {
        resultPath = aResultPath;
    }

    public static Date getCurentDate() {
        return new Date();
    }

    public static String getDefaultLogPath() {
        BootstrapManager bootstrapManager = getBootstrapManager();
        return bootstrapManager.getLocalHome() + System.getProperty("file.separator") + ConfluenceHomeGlobalConstants.LOGS_DIR;
    }

    public static void outputLog(String mes) {
        FileWriter fstream;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
            String strDate = sdf.format(getCurentDate());
            SimpleDateFormat sdfFile = new SimpleDateFormat("YYYY-MM");
            String strDateFileLog = sdfFile.format(getCurentDate());
            resultPath = getDefaultLogPath() + "\\" + MEMSOURCE_FOLDER_NAME + "\\" + MEMSOURCE_FILE_NAME + "." + strDateFileLog + "." + MEMSOURCE_FILE_EXTENSION;
            File file = new File(resultPath);
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
                if (!file.exists()) {
                    file.createNewFile();
                }
            }
            fstream = new FileWriter(resultPath, true);
            BufferedWriter fbw = new BufferedWriter(fstream);
            fbw.write(strDate + ": " + mes);
            fbw.newLine();
            fbw.close();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ManualLogUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static BootstrapManager getBootstrapManager() {
        return bootstrapManager;
    }

    public static void setBootstrapManager(BootstrapManager bootstrapManager) {
        ManualLogUtil.bootstrapManager = bootstrapManager;
    }
}
