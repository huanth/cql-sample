package imlv.vn.sample;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opensymphony.xwork.ActionContext;
import imlv.vn.sample.factory.ActionContextHelper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringEscapeUtils;

public class SearchAction extends ConfluenceActionSupport implements Beanable {

    private final String OPEN_BRACKET = "?";
    private final String CLOSE_BRACKET = "?";
    private PageManager pageManager;
    private SpaceManager spaceManager;
    Map<String, Object> map = new HashMap<String, Object>();

    @Override
    public Object getBean() {

        return map;
    }

    public String goToSearchPage() {
        return SUCCESS;
    }

    public String byCategoriesAndLabel() {
        JsonObject jsonObject;
        List<Label> labels;
        Page page;
        List<Page> listPage;
        List<Page> pagesResult = new ArrayList<Page>();
        Space space;
        List<Space> spaces;

        ActionContext context = ActionContext.getContext();
        String spaceCategory = ActionContextHelper.getFirstParameterValueAsString(context, "spaceCategory");
        spaceCategory = spaceCategory.toLowerCase();
        String pageLabel = ActionContextHelper.getFirstParameterValueAsString(context, "pageLabel");
        pageLabel = pageLabel.toLowerCase();
        List<String> pagesByTitle = ActionContextHelper.getParameterValuesAsJson(context, "pagesByTitle");

        if (pagesByTitle.isEmpty() || pagesByTitle == null || pagesByTitle.size() == 0) {
            if (spaceCategory.isEmpty() && pageLabel.isEmpty()) {

            }

            if (pageLabel.isEmpty() && !spaceCategory.isEmpty()) {
                spaces = spaceManager.getAllSpaces();
                for (Space spaceLoop : spaces) {
                    boolean flag = false;
                    labels = spaceLoop.getDescription().getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(spaceCategory)) {
                            flag = true;
                            break;
                        }
                    }
                    if (flag == true) {
                        listPage = pageManager.getPages(spaceLoop, true);
                        pagesResult.addAll(listPage);
                    }
                }
            }

            if (spaceCategory.isEmpty() && !pageLabel.isEmpty()) {
                spaces = spaceManager.getAllSpaces();
                for (Space spaceLoop : spaces) {
                    listPage = pageManager.getPages(spaceLoop, true);
                    for (Page pageLoop : listPage) {
                        boolean flag = false;
                        labels = pageLoop.getLabels();
                        for (Label labelLoop : labels) {
                            if (labelLoop.getName().contains("favourite")) {
                                continue;
                            }
                            if (labelLoop.getName().contains(pageLabel)) {
                                flag = true;
                                break;
                            }
                        }
                        if (flag == true) {
                            pagesResult.add(pageLoop);
                        }
                    }
                }
            }
            if (!spaceCategory.isEmpty() && !pageLabel.isEmpty()) {
                spaces = spaceManager.getAllSpaces();
                for (Space spaceLoop : spaces) {
                    int flag = 0;
                    labels = spaceLoop.getDescription().getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(spaceCategory)) {
                            flag += 1;
                            break;
                        }
                    }

                    if (flag == 1) {
                        listPage = pageManager.getPages(spaceLoop, true);
                        for (Page pageLoop : listPage) {
                            labels = pageLoop.getLabels();
                            for (Label labelLoop : labels) {
                                if (labelLoop.getName().contains("favourite")) {
                                    continue;
                                }
                                if (labelLoop.getName().contains(pageLabel)) {
                                    pagesResult.add(pageLoop);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        } else {
            if (pageLabel.isEmpty() && spaceCategory.isEmpty()) {
                for (String pageByTitle : pagesByTitle) {
                    jsonObject = new JsonParser().parse(pageByTitle).getAsJsonObject();
                    String pageID = jsonObject.get("id").getAsString();
                    page = pageManager.getPage(Long.parseLong(pageID));
                    pagesResult.add(page);
                }
            }
            if (!pageLabel.isEmpty() && spaceCategory.isEmpty()) {
                for (String pageByTitle : pagesByTitle) {
                    jsonObject = new JsonParser().parse(pageByTitle).getAsJsonObject();
                    String pageID = jsonObject.get("id").getAsString();
                    page = pageManager.getPage(Long.parseLong(pageID));
                    labels = page.getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(pageLabel)) {
                            pagesResult.add(page);
                            break;
                        }
                    }
                }
            }
            if (pageLabel.isEmpty() && !spaceCategory.isEmpty()) {
                for (String pageByTitle : pagesByTitle) {
                    jsonObject = new JsonParser().parse(pageByTitle).getAsJsonObject();
                    String pageID = jsonObject.get("id").getAsString();
                    page = pageManager.getPage(Long.parseLong(pageID));
                    space = page.getSpace();
                    labels = space.getDescription().getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(spaceCategory)) {
                            pagesResult.add(page);
                            break;
                        }
                    }
                }
            }
            if (!pageLabel.isEmpty() && !spaceCategory.isEmpty()) {
                for (String pageByTitle : pagesByTitle) {
                    int flag = 0;
                    jsonObject = new JsonParser().parse(pageByTitle).getAsJsonObject();
                    String pageID = jsonObject.get("id").getAsString();
                    page = pageManager.getPage(Long.parseLong(pageID));
                    labels = page.getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(pageLabel)) {
                            flag += 1;
                            break;
                        }
                    }
                    space = page.getSpace();
                    labels = space.getDescription().getLabels();
                    for (Label labelLoop : labels) {
                        if (labelLoop.getName().contains("favourite")) {
                            continue;
                        }
                        if (labelLoop.getName().contains(spaceCategory) && flag == 1) {
                            pagesResult.add(page);
                            break;
                        }
                    }
                }
            }
        }
        map.put("countPages", pagesResult.size());
        map.put("listPages", getRenderedDataSearch(pagesResult));
        return SUCCESS;
    }

    private String getRenderedDataSearch(List<Page> dataList) {
        I18NBean i18NBean = this.i18NBeanFactory.getI18NBean(getLocale());
        StringBuilder stringResult = new StringBuilder();

        if (dataList.isEmpty()) {
            stringResult.append("<option class=\"message-panel message-handler\" style='margin:10px;' disabled=\"disabled\">");
            stringResult.append(i18NBean.getText("imlv.vn.btx.view.searchbycqlsample.select.search.result.option"));
            stringResult.append("</option>");
            return stringResult.toString();
        }

        for (Page page : dataList) {
            Space space = page.getSpace();
            stringResult.append("<option data-pageId=\"");
            stringResult.append(page.getIdAsString());
            stringResult.append("\" value=\"");
            stringResult.append(page.getIdAsString());
            stringResult.append("\" data-spacekey=\"" + StringEscapeUtils.escapeHtml(space.getKey()))
                    .append("\"");
            stringResult.append(" data-page=\"")
                    .append(StringEscapeUtils.escapeHtml(page.getTitle()))
                    .append("\"");
            stringResult.append(">");
            stringResult.append("[ ")
                    .append(StringEscapeUtils.escapeHtml(space.getDisplayTitle()))
                    .append(" ] ")
                    .append(StringEscapeUtils.escapeHtml(page.getTitle()));
            stringResult.append("</option>");
        }
        return stringResult.toString();
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }
}
